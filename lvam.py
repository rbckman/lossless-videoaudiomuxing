#!/usr/bin/python
import os
import subprocess
import glob
import sys
import re
import time

i = 0

print ""
print "\033[1;34mVideoAudioMuxer script by rob using ffmpeg"
print "---------------------------------------------------------------"
print "Muxing together your Video and Audio files without any compressing"
print "---------------------------------------------------------------"
print "I am using it to make workflow easier while shooting high quality video and audio with Magic Lantern"
print "---------------------------------------------------------------"
print "Howto: Place this python script in your Video and Audio folder and run it!"
print "\033[1;m"

print "We are in path: ", sys.path[0]
print ""

## searching for files to be muxed
videomuxpath = sys.path[0]

audiofiler = glob.glob("%s/*.WAV" % (videomuxpath))
videofiler = glob.glob("%s/*.MOV" % (videomuxpath))
rawfiler = glob.glob("%s/*.dng" % (videomuxpath))


if len(rawfiler) > 0:
    print "Raw files detected... sorry! doesn't support this feature yet!"
if len(audiofiler) == 0:
    print "Sorry mate, didn't find any audios"
    print ""
    exit()
if len(videofiler) == 0:
    print "Sorry mate, didn't find any videos"
    print ""
    exit()

print "\033[1;35mAudio files to be muxed together: ", len(audiofiler), "\033[1;m"
print "\033[1;35mVideo files to be muxed together: ", len(videofiler), "\033[1;m"
print ""

## sorting the files in numerical order
order = sorted(audiofiler)
print "\033[1;31mFiles: ", order, "\033[1;m"
print ""

## getting the number from the first audio file (audio is one number lower than the videofile)
firstaudio = re.findall(r'\d+', order[0])
print "\033[1;36mNumber of the first audiofile: ", firstaudio, "\033[1;m"
print ""

## making firstfile to an int
audio = int(firstaudio[0])

askuser = raw_input('\033[1;41mDo you want to continue? (y)es or (n)o \033[1;m')

if askuser == 'n':
    print "Maybe next time! :)"
    exit()
else:
    print ""
    outputfilename = raw_input('Output filename?: ')
    print ""
    print "\033[1;44mOkey! Buckle your seatbelt dorothy cause kansas is going bye-bye\033[1;m"
    print ""
    time.sleep(2)
    while i < len(audiofiler):
	video = int(audio) + 1
	output = audio
	lista = ["ffmpeg", "-i", "MVI_%s.MOV"% (video), "-vcodec", "copy", "-i", "MVI_%s.WAV"%
	(audio), "-acodec", "copy", "%s_%s.MOV"% (outputfilename, output)]
	subprocess.call(lista)
	audio = audio + 1
	i = i + 1

print ""
print "Done! Thank you and long live ffmpeg & magic lantern!"
print ""
