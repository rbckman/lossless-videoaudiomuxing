
Lossless-VideoAudioMuxer using ffmpeg
------------------------------------------

Muxing(combine) together your Video and Audio files without any compressing.

I use it to make editing workflow easier while shooting high quality video and audio with Magic
Lantern. I'm filming with the Canon 7d and one of the nightly builds of Magic Lantern. Recording audio
separately as a WAV cause of better quality and also to see audio input levels while filming.

I don't know if every camera and every version of Magic Lantern outputs same order for the separate
audio and video files as with my 7d. It outputs the video file as one count higher, for example if
the video file is MVI_1001.MOV the audio file is MVI_1000.WAV.

What is Magic Lantern?
"Magic Lantern is a free software add-on that runs from the SD/CF card and adds a host of new
features to Canon EOS cameras that weren't included from the factory by Canon"
Check it out at: http://magiclantern.fm

Howto: Place the python script in your Video and Audio folder and run it!

Long live ffmpeg & Magic Lantern!

